package net.biomodels.jummp.model.converting.converters;

import net.biomodels.jummp.model.converting.Strategy;
import org.sbfc.converter.models.OctaveModel;
import org.sbfc.converter.models.SBMLModel;
import org.sbfc.converter.sbml2octave.SBML2Octave;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static net.biomodels.jummp.model.converting.ConverterFromSbml.resolveSbmlModel;

public class Sbml2Octave implements Strategy {
    @Override
    public FileInputStream convert(String inputModel, String outputModel) {
        SBMLModel sbmlModel = resolveSbmlModel(inputModel);
        OctaveModel octaveModel = new SBML2Octave().octaveExport(sbmlModel);
        System.out.println(octaveModel.modelToString());
        try {
            FileInputStream result = new FileInputStream(octaveModel.modelToString());
            return result;
        } catch (FileNotFoundException e) {

        }
        return null;
    }
}
