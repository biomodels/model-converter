package net.biomodels.jummp.model.converting.converters;

import net.biomodels.jummp.model.converting.Strategy;
import org.sbfc.converter.models.BioPaxModel;
import org.sbfc.converter.models.SBMLModel;
import org.sbfc.converter.sbml2biopax.SBML2BioPAX_l2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static net.biomodels.jummp.model.converting.ConverterFromSbml.resolveSbmlModel;

public class Sbml2BioPax implements Strategy {
    @Override
    public FileInputStream convert(String inputModel, String outputModel) {
        SBMLModel sbmlModel =
                resolveSbmlModel(inputModel);
        BioPaxModel bioPaxModel = new SBML2BioPAX_l2().biopaxexport(sbmlModel);
        System.out.println(bioPaxModel.modelToString());
        try {
            FileInputStream result = new FileInputStream(bioPaxModel.modelToString());
            return result;
        } catch (FileNotFoundException e) {

        }
        return null;
    }
}
