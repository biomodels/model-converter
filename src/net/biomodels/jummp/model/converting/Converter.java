package net.biomodels.jummp.model.converting;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Converter {
    private static final Logger logger = LoggerFactory.getLogger(Converter.class);
    public static void main(String[] args) {
        // we could assume context is already set by references
        logger.info("Model Converter");
        ConversionContext ctx = new ConverterFromSbml();
        ArrayList<String> models = new ArrayList<String>();
        models.add("resources/BIOMD0000000644_url.xml");
        models.add("matlab");
        ctx.convert(models);
    }
}
