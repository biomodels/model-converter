package net.biomodels.jummp.model.converting;

import java.io.FileInputStream;
import java.util.ArrayList;

public abstract class ConversionContext {
    Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    // use the strategy
    public void convert(ArrayList<String> models) {
        FileInputStream result = strategy.convert(models.get(0),  models.get(1));
        try {
            // reads till the end of the stream
            int i;
            char c;
            while((i = result.read())!=-1) {

                // converts integer to character
                c = (char)i;

                // prints character
                System.out.print(c);
            }
        } catch (Exception e) {

        }
    }

}
