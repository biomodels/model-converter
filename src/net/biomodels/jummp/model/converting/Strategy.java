package net.biomodels.jummp.model.converting;

import java.io.FileInputStream;

public interface Strategy {

    FileInputStream convert(String inputModel, String outputModel);
}
