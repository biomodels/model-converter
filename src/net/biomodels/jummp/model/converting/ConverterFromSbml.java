package net.biomodels.jummp.model.converting;

import net.biomodels.jummp.model.converting.converters.Sbml2BioPax;
import net.biomodels.jummp.model.converting.converters.Sbml2Matlab;
import net.biomodels.jummp.model.converting.converters.Sbml2Octave;
import org.sbfc.converter.models.SBMLModel;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.SBMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class ConverterFromSbml extends ConversionContext {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public ConverterFromSbml() {
        logger.info(this.getClass().toString());
        this.strategy = new Sbml2Octave(); //Sbml2BioPax(); //  new Sbml2Matlab();
    }

    /**
     * Resolves the SBMLModel from the given @p revision.
     * @param modelFile The RevisionTransportCommand from which to extract the SBMLModel.
     * @return The SBMLModel to be found or an empty array if the model could not be found.
     */
    public static SBMLModel resolveSbmlModel(String modelFile) {
        SBMLDocument doc;
        SBMLReader reader = new SBMLReader();
        try {
            doc = reader.readSBML(modelFile);
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
            String error = "SBMLDocument could not be read from " + modelFile;
            System.out.println(error);
            //log.error(error);
            return null;
        }

        try {
            Model model = doc.getModel();
            SBMLWriter sbmlWriter = new SBMLWriter();
            String sbmlString = sbmlWriter.writeSBMLToString(model.getSBMLDocument());
            SBMLModel sbmlModel = new SBMLModel();
            sbmlModel.setModelFromString(sbmlString);
            return sbmlModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
